import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './assets/css/tailwind.css'
import VModal from 'vue-js-modal'
import Notifications from 'vue-notification'
Vue.use(Notifications);
Vue.use(VModal );


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
