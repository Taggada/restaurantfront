import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/actus',
    name: 'Actus',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "actus" */ '../views/Actus.vue')
  },
  {
    path: '/apropos',
    name: 'Apropos',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "apropos" */ '../views/Propos.vue')
  },
  {
    path: '/contact',
    name: 'Contact',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "contact" */ '../views/Contact.vue')
  },
  {
    path: '/legal',
    name: 'Legales',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "legal" */ '../views/Legal.vue')
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue'),

  },
  {
    path: '/lacarte',
    name: 'Lacarte',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "lacarte" */ '../views/LaCarte.vue'),

  },
  {
    path: '/dashboard',
    name: "dashboard",
    component: () => import(/* webpackChunkName: "login" */ '../views/Dashboard.vue'),
    meta: {requiresAuth: true}
  },
  {
    path: '/platsdashboard',
    name: "platsdashboard",
    component: () => import(/* webpackChunkName: "login" */ '../views/Platdashboard.vue'),
    meta: {requiresAuth: true}
  },
  {
    path: '/editmenus',
    name: "editmenus",
    component: () => import(/* webpackChunkName: "login" */ '../views/Editdashboard.vue'),
    meta: {requiresAuth: true}
  }
]

const router = new VueRouter({
  routes
})
  router.beforeEach((to, from, next )=> {
    if(to.matched.some(record => record.meta.requiresAuth)){
      if(!localStorage.jwtToken){
        next({
          name: "Login"
        })
      }else{
        next();
      }
    }else{
      next();
    }
  });


export default router
