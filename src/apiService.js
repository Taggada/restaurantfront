//Fichier gérant les requêtes courante à l'API
import axios from 'axios';

function getMenus() { 
    axios.get('http://localhost:1337/menus').then((resp)=> {
        this.menus = resp.data
    })
    
}

function getEntrees(){
    axios.get('http://localhost:1337/type-de-plats/1').then((resp)=> {
        this.entrees = resp.data.plat
    })
}

function getPlats(){
    axios.get('http://localhost:1337/type-de-plats/2').then((resp)=> {
        return resp.data.plat
    })
}
function getDesserts(){
    axios.get('http://localhost:1337/type-de-plats/3').then((resp)=> {
        this.desserts = resp.data.plat
    })
}

async function getTypes(){

    let {resp} = await axios.get('http://localhost:1337/type-de-plats')
    return resp
}

function getDisplayMenu(id){
    axios.get('http://localhost:1337/display-menus/'+id).then((resp)=> {
        this.types = resp.data
    })
}

export{ getMenus, getEntrees, getPlats, getDesserts, getTypes, getDisplayMenu}