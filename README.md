# Restaurant les Amoureux 
## _Hey ! 👋  Retrouver ici le code de la partie front du restaurant-les-amoureux_

L'objectif de ce projet était de revoir de Zèro et 100% à la main le site déjà exitant sous WordPress
Créé à l'aide de framworks et outils moderne.

- VueJS 
- TailWinds CSS
- Beaucoup de café  

## Features
Le site à également une partie BackOffice interfacé avec [Strapi](https://strapi.io/)
- Création de plats (Entrées, plats, désserts, boisson ...)
- Création d'un menu avec Drag and Drop des plats
- Publication du Menu sur le site (Option possible publication Menu Enfant)

## Installation en local

Le site à besoin d'une version [Node.js](https://nodejs.org/) v10 ou supérieur et de la dernière version de [Yarn](https://yarnpkg.com/getting-started/install).

Pour lancer l'application en local.

```sh
cd restaurantfront
yarn 
yarn run serve
```


## Demo en Ligne 

Vous pouvez visiter la demo du site 
# [ici](http://dev.restaurant-les-amoureux.fr/#/)


